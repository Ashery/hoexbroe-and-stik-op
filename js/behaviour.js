jQuery(document).ready(function() {
    VideoLoad();


    jQuery(".fullscreen-icon").click(function(e) {
        toggleFullScreen();
    });

    // Add Loop Functionality
    jQuery(".btnloop").click(function(e) {
        jQuery(".btnloop").toggleClass("looped");
    });

    // Initialize Sticky 
    AddSticky();

    // Scroll Events
    jQuery(window).scroll(function() {
        AddSticky();
        VideoSmall();
    });

    $("#volume-slider").slider({
        min: 0,
        max: 100,
        value: 50,
        range: "min",
        slide: function(event, ui) {
            setVolume(ui.value / 100);
        }
    });

    jQuery("#volume-slider:before").click(function(e) {
        console.log("Hello Jesper");
    })

    // Smooth Scrolling
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function(e) {
            e.preventDefault();


            jQuery("body").removeClass("offcanvas-active");
            jQuery("header nav").removeClass("active");
            jQuery("#close-offcanvas").remove();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });

    // Responsive Off Canvas
    jQuery(".off-canvas").click(function(e) {
        if (jQuery("body").hasClass("offcanvas-active")) {
            jQuery("body").removeClass("offcanvas-active");
            jQuery("header nav").removeClass("active");
            jQuery("#close-offcanvas").remove();
        } else {
            jQuery("body").addClass("offcanvas-active");
            jQuery("header nav").addClass("active");
            jQuery("header nav ul").append("<i class='fas fa-times' id='close-offcanvas' aria-hidden='true'></i>");
            closeOffcanvas();
        }
    });

});



function setVolume(myVolume) {
    var video = jQuery("#video")[0];
    video.volume = myVolume;

    console.log(myVolume);
    if (myVolume == 0) {
        jQuery("#volume-slider").addClass("sound-disabled");
        jQuery("#volume-slider").removeClass("medium-sound");
    } else if (myVolume < 0.40) {
        jQuery("#volume-slider").addClass("medium-sound");
        jQuery("#volume-slider").removeClass("sound-disabled");
    } else if (myVolume > 0.40) {
        jQuery("#volume-slider").removeClass("medium-sound");
        jQuery("#volume-slider").removeClass("sound-disabled");
    } else {
        jQuery("#volume-slider").removeClass("sound-disabled");
        jQuery("#volume-slider").removeClass("medium-sound");
    }
}

function closeOffcanvas() {
    jQuery("#close-offcanvas").click(function(e) {
        jQuery("body").removeClass("offcanvas-active");
        jQuery("header nav").removeClass("active");
        jQuery("#close-offcanvas").remove();
    });
}


function toggleFullScreen() {

    if (!document.fullscreenElement && // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) { // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        jQuery("body").addClass("fullscreen");
        jQuery("#fullscreen").removeClass("fa-expand");
        jQuery("#fullscreen").addClass("fa-compress");
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        jQuery("body").removeClass("fullscreen");
        jQuery("#fullscreen").addClass("fa-expand");
        jQuery("#fullscreen").removeClass("fa-compress");
    }
}


function VideoSmall() {
    // Small Video Event Checker
    var fixedElement = jQuery("#intro");
    var windowPosition = jQuery(window).scrollTop();
    var elementPosition = jQuery("#upcoming-shows").offset().top;

    if (windowPosition > elementPosition) {
        fixedElement.addClass("fixed");

        if (jQuery(".video-wrapper").hasClass("video-active")) {
            jQuery(".video-wrapper").addClass("video-small");
            jQuery("header").css('opacity', '1');
        }
    } else {
        fixedElement.removeClass("fixed");

        if (jQuery(".video-wrapper").hasClass("video-active")) {
            jQuery(".video-wrapper").removeClass("video-small");

            jQuery("header").css('opacity', '0.15');
        }
    }
}

function AddSticky() {
    var fixedElement = jQuery("header");
    var windowPosition = jQuery(window).scrollTop();
    var elementPosition = jQuery("#upcoming-shows").offset().top;
    var logo = jQuery(".logo");
    var headerlogo = jQuery(".header-logo");

    // If the scrolling goes beneath the intro section, header becomes sticky
    if (windowPosition > elementPosition) {
        fixedElement.addClass("fixed");
        logo.fadeOut();
        headerlogo.fadeIn();
    } else {
        fixedElement.removeClass("fixed");
        logo.fadeIn();
        headerlogo.fadeOut("");
    }
};

function VideoLoad() {

    var video = jQuery("#video")[0];

    video.ontimeupdate = function() {

        // Seek Bar
        var percentage = (video.currentTime / video.duration) * 100;
        jQuery(".progress").css("width", percentage + "%");

        jQuery(".played").text(formatSecondsAsTime(video.currentTime));

        if (percentage == 100) {

            // When Video is Done, hide pause, and show play button to restart
            jQuery(".pause").hide();
            jQuery(".play").show();
            video.pause();

            setInterval(VideoLoop(video), 3000);
        }
    }

    jQuery(".scrubber").on("click", function(e) {

        progress = jQuery(".progress");

        var offset = $(".progress").offset();
        console.log(offset);
        var left = (e.pageX - offset.left);
        var totalWidth = jQuery(".progress").width();
        var percentage = (left / totalWidth);
        var videoTime = video.duration * percentage;
        video.currentTime = videoTime;
        jQuery(".play").show();
        jQuery(".played").text(formatSecondsAsTime(video.currentTime));
        // Show Pause Button
        jQuery(".pause").hide();
    });


    VideoControls();


}



function VideoLoop(video) {
    // What Happens If Video is Looped
    if (jQuery(".btnloop").hasClass("looped")) {
        video.play();
        jQuery(".pause").show();
        jQuery(".play").hide();

    }
}

function formatSecondsAsTime(secs, format) {
    // Format the time of the video
    var hr = Math.floor(secs / 3600);
    var min = Math.floor((secs - (hr * 3600)) / 60);
    var sec = Math.floor(secs - (hr * 3600) - (min * 60));

    if (min < 10) {
        min = "0" + min;
    }
    if (sec < 10) {
        sec = "0" + sec;
    }

    return min + ':' + sec;
}

function VideoControls() {


    video_playing = false;
    var video = jQuery("#video");

    jQuery(".play").click(function(e) {
        // Find Video


        jQuery(".tooltip").hide();
        // Fade Out Header
        jQuery("header").fadeTo("slow", 0.15, function() {});

        // Show Video
        video.show();

        // Add active class to wrapper
        jQuery(".video-wrapper").addClass("video-active");

        // Play Video
        video[0].play();

        // Hide Play Button
        jQuery(".play").hide();

        // Show Pause Button
        jQuery(".pause").show();


        AddSticky();

    });

    jQuery(video).click(function(e) {
        if (video[0].paused) {
            // Play Video
            video[0].play();
            // Hide  Pause Button
            jQuery(".pause").show();
            // Show Play Button
            jQuery(".play").hide();
        } else {
            video[0].pause();
            // Hide  Pause Button
            jQuery(".pause").hide();
            // Show Play Button
            jQuery(".play").show();
        }
    })

    jQuery(".pause").click(function(e) {
        // Play Video
        video[0].pause();

        // Fade In Header
        jQuery("header").fadeTo("slow", 1, function() {});

        // Hide  Pause Button
        jQuery(".pause").hide();

        // Show Play Button
        jQuery(".play").show();
    });




    jQuery(".close-icon").on("click", function() {

        // Fade In Header
        jQuery("header").fadeTo("slow", 1, function() {});

        // Add active class to wrapper
        jQuery(".video-wrapper").removeClass("video-active");
        jQuery(".video-wrapper").removeClass("video-small");

        // Play Video
        video[0].pause();

        // Hide Video
        video.hide();

        // Hide  Pause Button
        jQuery(".pause").hide();

        // Show Play Button
        jQuery(".play").show();
    });
}